cmake_minimum_required(VERSION 3.10)
project(robotworld)

find_package(Boost REQUIRED COMPONENTS system)
find_package(wxWidgets REQUIRED)

get_filename_component(PROJECT_CONFIG "../cmake.cfg" REALPATH BASE_DIR "${CMAKE_BINARY_DIR}")

set(CMAKE_CXX_FLAGS "-Wall -Wconversion -Wextra")

if(EXISTS ${PROJECT_CONFIG})
    file(READ cmake.cfg WX_FLAGS)
    string(REGEX REPLACE "\n$" "" WX_FLAGS "${WX_FLAGS}")

    
    set(CMAKE_CXX_FLAGS "${WX_FLAGS} ${CMAKE_CXX_FLAGS}")

    file(GLOB src "src/*.cpp")
    file(GLOB kalmanSrc "src/Kalman/*.cpp")
    file(GLOB particleSrc "src/Particle/*.cpp")

    add_executable(robotworld ${src}
    ${kalmanSrc}
    ${particleSrc}
    )
    include_directories(${Boost_INCLUDE_DIRS} ${wxWidgets_INCLUDE_DIRS})
    target_link_libraries(robotworld ${Boost_LIBRARIES} ${wxWidgets_LIBRARIES})
else()
    set(PRE_MSG "-- ")
    set(ERROR_MSG "ERROR: cmake config file for usage of wxWidgets not found, run ./configure in base dir")
    if(NOT WIN32)
        string(ASCII 27 Esc)
        set(Red "${Esc}[31m")
        set(ColourReset "${Esc}[m")
        message("${PRE_MSG}${Red}${ERROR_MSG} ${ColourReset}")
    else()
        message("${PRE_MSG}${ERROR_MSG}")
    endif()
endif()

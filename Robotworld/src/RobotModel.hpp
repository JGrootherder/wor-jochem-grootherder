#ifndef MODEL_HPP__
#define MODEL_HPP__

#include "Matrix.hpp"
#include <memory>

class RobotModel;
typedef std::shared_ptr<RobotModel> RobotModelPtr; 

class RobotModel
{
public:
    RobotModel(){};
    virtual ~RobotModel(){};
    virtual void updateBelief(Matrix<double,2,1> processNoise, Matrix<double,2,1> measurement, Matrix<double,2,1>){};
    virtual const Matrix<double,2,1> getBelief_matrix() const{};
};

#endif
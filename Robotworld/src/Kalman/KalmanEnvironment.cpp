#include "KalmanEnvironment.hpp"
#include <iostream>
KalmanEnvironment::KalmanEnvironment(KalmanFilter* kalmanFilter, Matrix<double,2,1>startPosition) : kalmanFilter(kalmanFilter), currentState(startPosition),
    compass(KalmanCompass(startPosition.at(0,0),startPosition.at(1,0))),
    odometer(KalmanOdometer(startPosition.at(0,0), startPosition.at(1,0)))
{
}

KalmanEnvironment::~KalmanEnvironment()
{

}

void KalmanEnvironment::doAction(const Matrix<double,2,1>& belief)
{
    Matrix<double,2,1> state = (kalmanFilter->get_a_matrix() * currentState) + (kalmanFilter->get_b_matrix() * kalmanFilter->getAction_matrix());
    previousState = currentState;

    generateProcessNoise();
    currentState = state + processNoise_matrix;
    updateMeasurement();
}

void KalmanEnvironment::updateMeasurement()
{
     Matrix<double, 2,1> previousMeasurement = previousState;
    Matrix<double,2,1> randomValues;

    kalmanFilter->generateNDValue(randomValues, 0, 0, 0, 2);
    kalmanFilter->generateNDValue(randomValues, 1, 0, 0, 1);
    
    measureNoise_matrix.at(0,0) = randomValues.at(1,0) * sin(randomValues.at(0,0) * M_PI / 180);
    measureNoise_matrix.at(1,0) = randomValues.at(1,0) * cos(randomValues.at(0,0) * M_PI / 180);

    double angle = compass.getCompassValue(currentState);
    double distance = odometer.getOdometerValue(currentState);

    previousMeasurement.at(0,0) += distance * cos(angle * M_PI /180);
    previousMeasurement.at(1,0) += distance * sin(angle * M_PI / 180);

    measurement_matrix = kalmanFilter->get_c_matrix() * previousMeasurement + measureNoise_matrix;

}

void KalmanEnvironment::generateProcessNoise()
{
    unsigned short randomValue = kalmanFilter->generateUDValue(0, 100);
    if(randomValue <= 70)
    {
        kalmanFilter->generateNDValue(processNoise_matrix, 0, 0, 0, 1);
        kalmanFilter->generateNDValue(processNoise_matrix, 1, 0, 0, 1);
    }
    else
    {
        kalmanFilter->generateNDValue(processNoise_matrix, 0, 0, 0, 2);
        kalmanFilter->generateNDValue(processNoise_matrix, 1, 0, 0, 2);
    }
}

const Matrix<double,2,1> KalmanEnvironment::getCurrentState() const
{
    return currentState;
}

const Matrix<double,2,1> KalmanEnvironment::getMeasurement_matrix() const
{
    return measurement_matrix;
}

const Matrix<double,2,1> KalmanEnvironment::getProcessNoise_matrix() const
{
    return processNoise_matrix;
}

const Matrix<double,2,1> KalmanEnvironment::getMeasurementNoise_matrix() const
{
    return measureNoise_matrix;
}
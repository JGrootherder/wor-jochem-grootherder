#ifndef KALMAN_ENVIRONMENT_HPP
#define KALMAN_ENVIRONMENT_HPP

#include "Kalman.hpp"
#include "KalmanCompass.hpp"
#include "KalmanOdometer.hpp"
#include "../Environment.hpp"

class KalmanEnvironment : public Environment
{
public:
    KalmanEnvironment(KalmanFilter* kalmanFilter, Matrix<double,2,1>startPosition);
    virtual ~KalmanEnvironment();

    void doAction(const Matrix<double,2,1>& belief);
    void updateMeasurement();
    void generateProcessNoise();
    const Matrix<double,2,1> getCurrentState() const;
    const Matrix<double,2,1> getMeasurement_matrix() const;
    const Matrix<double, 2,1> getProcessNoise_matrix() const;
    const Matrix<double, 2,1> getMeasurementNoise_matrix() const;
private:
    KalmanFilter* kalmanFilter;
    Matrix<double ,2,1> currentState;
    Matrix<double ,2,1> previousState;
    Matrix<double, 2,1> measurement_matrix;
    Matrix<double, 2,1> processNoise_matrix;
    Matrix<double, 2,1> measureNoise_matrix;
    KalmanCompass compass;
    KalmanOdometer odometer;
};

#endif
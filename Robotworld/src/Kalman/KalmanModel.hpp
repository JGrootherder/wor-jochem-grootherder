#ifndef KALMAN_MODEL_HPP__
#define KALMAN_MODEL_HPP__

#include "Kalman.hpp"
#include "../RobotModel.hpp"

class KalmanModel : public RobotModel
{
public:
    KalmanModel(KalmanFilter* kalmanFilter, Matrix<double,2,1>startPosition);
    virtual ~KalmanModel();

    void updateBelief(Matrix<double,2,1> processNoise, Matrix<double,2,1> measurement, Matrix<double,2,1>);
    virtual const Matrix<double,2,1> getBelief_matrix() const;
private:
    KalmanFilter* kalmanFilter;
    Matrix<double,2,1> belief_matrix;
    Matrix<double,2,2> variance_matrix = {{1,0}, {0,1}};
};

#endif
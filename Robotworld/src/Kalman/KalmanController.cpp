#include "KalmanController.hpp"
#include <iostream>
KalmanController::KalmanController(const Point& position) :  
    model(std::make_shared<KalmanModel>(&kalmanFilter, Matrix<double,2,1>{position.x, position.y})),
    environment(std::make_shared<KalmanEnvironment>(&kalmanFilter, Matrix<double,2,1>{position.x, position.y})) 
{
    previousPosition = position;   
}

KalmanController::~KalmanController()
{

}

void KalmanController::update(const Point& newPosition)
{ 
    double differenceX = newPosition.x - previousPosition.x;
    double differenceY = newPosition.y - previousPosition.y;
    previousPosition = newPosition;
    Matrix<double,2,1> action;
    action.at(0,0) = differenceX;
    action.at(1,0) = differenceY;
    kalmanFilter.setAction_matrix(action);
}

void KalmanController::run(const Point& newPosition)
{
    update(newPosition);
    environment->doAction(model->getBelief_matrix());
    environment->updateMeasurement();
    model->updateBelief(environment->getProcessNoise_matrix(), environment->getMeasurement_matrix(), environment->getMeasurementNoise_matrix());
}


const RobotModelPtr KalmanController::getModel() const
{
    return model;
}

const EnvironmentPtr KalmanController::getEnvironment() const
{
    return environment;
}

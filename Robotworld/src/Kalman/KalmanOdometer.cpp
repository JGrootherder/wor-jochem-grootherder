#include "KalmanOdometer.hpp"
#include <cmath>


KalmanOdometer::KalmanOdometer(double x, double y) : oldXPosition(x), oldYPosition(y)
{

}


KalmanOdometer::~KalmanOdometer()
{

}

double KalmanOdometer::getOdometerValue(const Matrix<double,2,1>& newPosition)
{
    double newX = newPosition.at(0,0);
    double newY = newPosition.at(1,0);

    double distanceDriven = sqrt(pow(oldXPosition - newX,2) + pow(oldYPosition - newY, 2));
    oldXPosition = newX;
    oldYPosition = newY;
    return distanceDriven;
}
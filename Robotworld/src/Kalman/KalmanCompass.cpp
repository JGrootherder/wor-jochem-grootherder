#include "KalmanCompass.hpp"
#include <cmath>

KalmanCompass::KalmanCompass(double x, double y) : oldXPosition(x), oldYPosition(y)
{

}

KalmanCompass::~KalmanCompass()
{

}

double KalmanCompass::getCompassValue(const Matrix<double,2,1>& newPosition)
{
    double changeX = newPosition.at(0,0) - oldXPosition;
    double changeY = newPosition.at(1,0) - oldYPosition;

    oldXPosition = newPosition.at(0,0);
    oldYPosition = newPosition.at(1,0);

    double result = atan2(changeY, changeX) * 180 / M_PI;

    if(result < 90)
    {
        result =  (result - 90) *-1;
    } 
    else
    {
        result = 360 - (result - 90);
    }
    
    return result;
}
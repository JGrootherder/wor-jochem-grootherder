#ifndef KALMAN_COMPASS_HPP__
#define KALMAN_COMPASS_HPP__

#include "../Matrix.hpp"

class KalmanCompass
{
public:
    KalmanCompass(double x, double y);
    virtual ~KalmanCompass();

    double getCompassValue(const Matrix<double,2,1>& newPosition);

private:
    double oldXPosition;
    double oldYPosition;
};

#endif
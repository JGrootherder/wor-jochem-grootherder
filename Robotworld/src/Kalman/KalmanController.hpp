#ifndef KALMAN_CONTROLLER_HPP__
#define KALMAN_CONTROLLER_HPP__

#include "KalmanEnvironment.hpp"
#include "KalmanModel.hpp"
#include "Kalman.hpp"
#include "../Point.hpp"
#include "../RobotController.hpp"
#include <memory>

class KalmanController : public RobotController
{
public:
    // KalmanController(KalmanFilter kalmanFilter);
    KalmanController(const Point& position);
    ~KalmanController();

    const RobotModelPtr getModel() const;
    const EnvironmentPtr getEnvironment() const;

    void update(const Point& newPosition);
    void run(const Point& newPosition);
private:
    KalmanFilter kalmanFilter;
    Point previousPosition;
    RobotModelPtr model;
    EnvironmentPtr environment;
};



#endif
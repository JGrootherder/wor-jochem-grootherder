#include "KalmanModel.hpp"
#include <iostream>

KalmanModel::KalmanModel(KalmanFilter* kalmanFilter, Matrix<double,2,1>startPosition) : kalmanFilter(kalmanFilter), belief_matrix(startPosition)
{
}

KalmanModel::~KalmanModel()
{

}

void KalmanModel::updateBelief(Matrix<double,2,1> processNoise, Matrix<double,2,1> measurement, Matrix<double,2,1> measurementNoise)
{
    //Prediction of mean
    Matrix<double,2,1> _u = kalmanFilter->get_a_matrix() * belief_matrix + kalmanFilter->get_b_matrix() * kalmanFilter->getAction_matrix();

    //Prediction of variance
    Matrix<double,2,2> R = processNoise * processNoise.transpose();
    Matrix<double,2,2> _E = kalmanFilter->get_a_matrix() * variance_matrix * kalmanFilter->get_a_matrix().transpose() + R; 

    //Kalman gain

    Matrix<double,2,2> Q = measurementNoise * measurementNoise.transpose();
    Matrix<double,2,2> K = _E * kalmanFilter->get_c_matrix().transpose() * (kalmanFilter->get_c_matrix() * _E * kalmanFilter->get_c_matrix().transpose() + Q).inverse();

    //Belief of mean
    belief_matrix = _u + K * (measurement - kalmanFilter->get_c_matrix() * _u);
    //Belief of variance
    variance_matrix = (_E.identity()-K * kalmanFilter->get_c_matrix()) * _E;

}

const Matrix<double,2,1> KalmanModel::getBelief_matrix() const
{
    return belief_matrix;
}

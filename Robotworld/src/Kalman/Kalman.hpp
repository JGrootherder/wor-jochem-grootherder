#ifndef KALMAN_HPP__
#define KALMAN_HPP__

#include "../Matrix.hpp"

class KalmanFilter
{
public:
    KalmanFilter();
    virtual ~KalmanFilter();
    void generateNDValue(Matrix<double,2,1>& input, uint8_t row, uint8_t column, double measurementValue, double deviation);
    unsigned short generateUDValue(unsigned short min, unsigned short max);

    const Matrix<double,2,2> get_a_matrix() const;
    const Matrix<double,2,2> get_b_matrix() const;
    const Matrix<double,2,2> get_c_matrix() const;
    void setAction_matrix(Matrix<double,2,1> anAction_matrix);
    const Matrix<double,2,1> getAction_matrix() const;

private:
    Matrix<double, 2,2> a_matrix; 
    Matrix<double, 2,2> b_matrix; 
    Matrix<double, 2,2> c_matrix;
    // Matrix<double, 2,1> action_matrix = {{0, 0}}; 
    Matrix<double,2,1> action_matrix; 
};

#endif
#ifndef KALMAN_ODOMETER_HPP
#define KALMAN_ODOMETER_HPP

#include "../Matrix.hpp"

class KalmanOdometer
{
public:
    KalmanOdometer(double x, double y);
    virtual ~KalmanOdometer();

    double getOdometerValue(const Matrix<double, 2,1>& newPosition);

private:
    double oldXPosition;
    double oldYPosition;
};

#endif
#include "Kalman.hpp"
#include <random>
#include <iostream>

KalmanFilter::KalmanFilter()
{
    action_matrix.at(0,0) = 0;
    action_matrix.at(1,0) = 0;
    a_matrix = {{1,0}, {0,1}};
    b_matrix = {{1,0}, {0,1}};
    c_matrix = {{1,0}, {0,1}};
}

KalmanFilter::~KalmanFilter()
{

}

void KalmanFilter::generateNDValue(Matrix<double, 2,1>& input, uint8_t row, uint8_t column, double measurementValue, double deviation)
{
    std::random_device rd{};
    std::mt19937 gen{rd()};
    std::normal_distribution<> d{measurementValue, deviation};
    input.at(row,column) = d(gen);
}

unsigned short KalmanFilter::generateUDValue(unsigned short min, unsigned short max)
{
    std::random_device rd{};
    std::mt19937 gen{rd()};
    std::uniform_int_distribution<unsigned short> d(min, max);
    return d(gen);

}

const Matrix<double,2,2> KalmanFilter::get_a_matrix() const
{
    return a_matrix;
}
const Matrix<double,2,2> KalmanFilter::get_b_matrix() const
{
   return b_matrix;
}
const Matrix<double,2,2> KalmanFilter::get_c_matrix() const
{
    return c_matrix;
}
void KalmanFilter::setAction_matrix(Matrix<double,2,1>anAction_matrix)
{
    action_matrix = anAction_matrix;
}
const Matrix<double,2,1> KalmanFilter::getAction_matrix() const
{
    return action_matrix;
}

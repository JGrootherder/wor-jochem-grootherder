#ifndef ENVIRONMENT_HPP__
#define ENVIRONMENT_HPP__

#include "Matrix.hpp"
#include <memory>

class Environment;
typedef std::shared_ptr<Environment> EnvironmentPtr;


class Environment
{
public:
    Environment(){};
    virtual ~Environment(){};

    virtual void doAction(const Matrix<double,2,1>& belief){};
    virtual const Matrix<double,2,1> getCurrentState() const{};
    virtual void updateMeasurement(){};
    virtual void generateProcessNoise(){};
    virtual const Matrix<double,2,1> getMeasurement_matrix() const{};
    virtual const Matrix<double, 2,1> getProcessNoise_matrix() const{};
    virtual const Matrix<double, 2,1> getMeasurementNoise_matrix() const{};
};


#endif
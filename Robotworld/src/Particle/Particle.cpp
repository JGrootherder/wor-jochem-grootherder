#include "Particle.hpp"

ParticleFilter::ParticleFilter()
{

}

ParticleFilter::~ParticleFilter()
{

}


Matrix<double,2,1> ParticleFilter::getAction_matrix()
{
    return action_matrix;
}

void ParticleFilter::setAction_matrix(const Matrix<double,2,1>& anAction_matrix)
{
    action_matrix = anAction_matrix;
}

unsigned short ParticleFilter::generateUDValue(unsigned short max)
{
     std::random_device rd{};
    std::mt19937 gen{rd()};
    std::uniform_int_distribution<unsigned short> d(0, max);
    return d(gen);
}
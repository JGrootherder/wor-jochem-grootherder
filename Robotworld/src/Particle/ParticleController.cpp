#include "ParticleController.hpp"

ParticleController::ParticleController(const Point& position) : 
    model(std::make_shared<ParticleModel>(&particleFilter)),
    environment(std::make_shared<ParticleEnvironment>(&particleFilter, position))
{

}

ParticleController::~ParticleController()
{

}

void ParticleController::run(const Point& position)
{

}

void ParticleController::update(const Point& position)
{

}

#ifndef PARTICLE_MODEL_HPP__
#define PARTICLE_MODEL_HPP__

#include <vector>
#include "Particle.hpp"
#include "../RobotModel.hpp"

class ParticleModel : public RobotModel
{
public:
    ParticleModel(ParticleFilter* particleFilter);
    virtual ~ParticleModel();

    virtual void updateBelief(const Matrix<double,2,1>& action, const Matrix<double,2,1>& measurement);
    virtual const Matrix<double,2,1> getBelief_matrix() const;

private:
    void initialiseParticles();

    ParticleFilter* particleFilter;
    Matrix<double,2,1> belief_matrix;
    std::vector<Particle> belief;
    std::vector<Particle> prediction;
};

#endif
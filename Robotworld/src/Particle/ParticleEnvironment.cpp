#include "ParticleEnvironment.hpp"

ParticleEnvironment::ParticleEnvironment(ParticleFilter* particleFilter, const Point& position) :
    particleFilter(particleFilter)
{
    currentState.at(0,0) = position.x;
    currentState.at(1,0) = position.y;
    previousState = currentState;
}

ParticleEnvironment::~ParticleEnvironment()
{

}

void ParticleEnvironment::doAction(const Matrix<double,2,1>&belief)
{

}

const Matrix<double,2,1> ParticleEnvironment::getCurrentState() const
{

}
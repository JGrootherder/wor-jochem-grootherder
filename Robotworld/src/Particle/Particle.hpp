#ifndef PARTICLE_HPP__
#define PARTICLE_HPP__

#include <random>
#include "../Point.hpp"
#include "../Matrix.hpp"

struct Particle
{
    Matrix<unsigned short,2,1> position;
    double weight;

    Particle(const Matrix<unsigned short,2,1>& position, double weight = 0) : position(position), weight(weight)
    {
    }

    virtual ~Particle()
    {
    }

    
};

class ParticleFilter
{
public:
    ParticleFilter();
    virtual ~ParticleFilter();

    Matrix<double,2,1> getAction_matrix();
    void setAction_matrix(const Matrix<double,2,1>& anAction_matrix);
    unsigned short generateUDValue(unsigned short max);

private:
    Matrix<double,2,1> action_matrix;

};

#endif
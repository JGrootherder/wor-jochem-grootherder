#ifndef PARTICLE_ENVIRONMENT_HPP__
#define PARTICLE_ENVIRONMENT_HPP__

#include "Particle.hpp"
#include "../Point.hpp"
#include "../Environment.hpp"

class ParticleEnvironment : public Environment
{
public:
    ParticleEnvironment(ParticleFilter* particleFilter, const Point& position);
    virtual ~ParticleEnvironment();

    void doAction(const Matrix<double,2,1>&belief);
    const Matrix<double,2,1> getCurrentState() const;

private:
    ParticleFilter* particleFilter;
    Matrix<double,2,1> currentState;
    Matrix<double,2,1> previousState;
};

#endif
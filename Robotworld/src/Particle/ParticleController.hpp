#ifndef PARTICLE_CONTROLLER_HPP__
#define PARTICLE_CONTROLLER_HPP__
#include <memory>

#include "../RobotController.hpp"
#include "../Point.hpp"
#include "Particle.hpp"
#include "ParticleModel.hpp"
#include "ParticleEnvironment.hpp"

class ParticleController : public RobotController
{
public:
    ParticleController(const Point& position);
    virtual ~ParticleController();

    void run(const Point& position);
    void update(const Point& position);

private:
    ParticleFilter particleFilter;
    Point previousPosition;
    RobotModelPtr model;
    EnvironmentPtr environment;
};

#endif
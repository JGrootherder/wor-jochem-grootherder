#include "ParticleModel.hpp"

#define AMOUNT_OF_PARTICLES 700
#define X_MAX 1024
#define Y_MAX 1024

ParticleModel::ParticleModel(ParticleFilter* particleFilter) : particleFilter(particleFilter)
{
    initialiseParticles();
}

ParticleModel::~ParticleModel()
{

}


void ParticleModel::updateBelief(const Matrix<double,2,1>& action, const vector<double>& measurements)
{
    prediction = belief;
    for(unsigned short i = 0; i  < prediction.size(); i++)
    {
        prediction.at(i).position += action + generateProcessNoise();
    }

}

const Matrix<double,2,1> ParticleModel::getBelief_matrix() const
{

}

void ParticleModel::initialiseParticles()
{
    for(unsigned short i = 0; i < AMOUNT_OF_PARTICLES; ++i)
    {
        Matrix<unsigned short,2,1> particlePosition = {{particleFilter->generateUDValue(X_MAX)}, {{particleFilter->generateUDValue(Y_MAX)}}};
        Particle particle(particlePosition, 1);
        belief.push_back(particle);
    }
}

Matrix<double,2,1> ParticleModel::generateProcessNoise()
{
    Matrix<double,2,1> processNoise;
    processNoise.at(0,0) = generateNDValue();
    processNoise.at(1,0) = generateNDValue();
    return processNoise();
}

double ParticleModel::generateNDValue()
{
    std::random_device rd{};
    std::mt19937 gen{rd()};
    std::normal_distribution<> d{0, 1};
    return d(gen);
}
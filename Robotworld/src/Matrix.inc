#include <cassert>
#include <stdexcept>
#include <numeric>
#include <cmath>
#include <utility>
#include <iomanip>

/**
 *
 */
template< class T, std::size_t M, std::size_t N >
Matrix< T, M, N >::Matrix( T value)
{
	for (std::size_t row = 0; row < M; ++row)
	{
		for (std::size_t column = 0; column < N; ++column)
		{
			matrix.at( row).at( column) = value;
		}
	}
}
/**
 *
 */
template< class T, std::size_t M, std::size_t N >
Matrix< T, M, N >::Matrix( const std::initializer_list< T >& aList)
{
	// Check the arguments
	assert( aList.size() == M * N);

	auto row_iter = aList.begin();
	for (std::size_t row = 0; row < M; ++row)
	{
		for (std::size_t column = 0; column < N; ++column, ++row_iter)
		{
			matrix.at( row).at( column) = *row_iter;
		}
	}
}
/**
 *
 */
template< class T, std::size_t M, std::size_t N >
Matrix< T, M, N >::Matrix( const std::initializer_list< std::initializer_list< T > >& aList)
{
	// Check the arguments, the static assert assures that there is at least 1 M and 1 N!
	assert( aList.size() == M && (*aList.begin()).size() == N);

	auto row_iter = aList.begin();
	for (std::size_t row = 0; row < aList.size(); ++row, ++row_iter)
	{
		auto column_iter = (*row_iter).begin();
		for (std::size_t column = 0; column < (*row_iter).size(); ++column, ++column_iter)
		{
			matrix.at( row).at( column) = *column_iter;
		}
	}
}
/**
 *
 */
template< class T, std::size_t M, std::size_t N >
Matrix< T, M, N >::Matrix( const Matrix< T, M, N >& aMatrix) :
				matrix( aMatrix.matrix)
{
}
/**
 *
 */
template< class T, std::size_t M, std::size_t N >
std::array< T, N >& Matrix< T, M, N >::at( std::size_t aRowIndex)
{
	return matrix.at( aRowIndex);
}
/**
 *
 */
template< class T, std::size_t M, std::size_t N >
const std::array< T, N >& Matrix< T, M, N >::at( std::size_t aRowIndex) const
{
	return matrix.at( aRowIndex);
}
/**
 *
 */
template< class T, std::size_t M, std::size_t N >
T& Matrix< T, M, N >::at( 	std::size_t aRowIndex,
							std::size_t aColumnIndex)
{
	return matrix.at( aRowIndex).at( aColumnIndex);
}
/**
 *
 */
template< class T, std::size_t M, std::size_t N >
const T& Matrix< T, M, N >::at( std::size_t aRowIndex,
								std::size_t aColumnIndex) const
{
	return matrix.at( aRowIndex).at( aColumnIndex);
}
/**
 *
 */
template< class T, std::size_t M, std::size_t N >
std::array< T, N >& Matrix< T, M, N >::operator[]( std::size_t aRowIndex)
{
	return matrix[aRowIndex];
}
/**
 *
 */
template< class T, std::size_t M, std::size_t N >
const std::array< T, N >& Matrix< T, M, N >::operator[]( std::size_t aRowIndex) const
{
	return matrix[aRowIndex];
}
/**
 *
 */
template< class T, std::size_t M, std::size_t N >
Matrix< T, M, N >& Matrix< T, M, N >::operator=( const Matrix< T, M, N >& rhs)
{
	if (this != &rhs)
	{
		matrix = rhs.matrix;
	}
	return *this;
}
/**
 *
 */
template< class T, std::size_t M, std::size_t N >
bool Matrix< T, M, N >::operator==( const Matrix< T, M, N >& rhs) const
{
	return matrix == rhs.matrix;
}
/**
 *
 */
template< class T, std::size_t M, std::size_t N >
template< class T2 >
Matrix< T, M, N >& Matrix< T, M, N >::operator*=( const T2& scalar)
{
	static_assert( std::is_arithmetic<T2>::value, "Value T2 must be arithmetic, see http://en.cppreference.com/w/cpp/types/is_arithmetic");

	for (std::size_t row = 0; row < M; ++row)
	{
		for (std::size_t column = 0; column < N; ++column)
		{
			matrix.at( row).at( column) *= scalar;
		}
	}
	return *this;
}
/**
 *
 */
template< class T, std::size_t M, std::size_t N >
template< class T2 >
Matrix< T, M, N > Matrix< T, M, N >::operator*( const T2& scalar) const
{
	static_assert( std::is_arithmetic<T2>::value, "Value T2 must be arithmetic, see http://en.cppreference.com/w/cpp/types/is_arithmetic");

	Matrix< T, M, N > result( *this);
	return result *= scalar;
}
/**
 *
 */
template< class T, std::size_t M, std::size_t N >
template< class T2 >
Matrix< T, M, N >& Matrix< T, M, N >::operator/=( const T2& aScalar)
{
	static_assert( std::is_arithmetic<T2>::value, "Value T2 must be arithmetic, see http://en.cppreference.com/w/cpp/types/is_arithmetic");

	for (std::size_t row = 0; row < M; ++row)
	{
		for (std::size_t column = 0; column < N; ++column)
		{
			matrix.at( row).at( column) /= aScalar;
		}
	}
	return *this;
}
/**
 *
 */
template< class T, std::size_t M, std::size_t N >
template< class T2 >
Matrix< T, M, N > Matrix< T, M, N >::operator/( const T2& aScalar) const
{
	static_assert( std::is_arithmetic<T2>::value, "Value T2 must be arithmetic, see http://en.cppreference.com/w/cpp/types/is_arithmetic");

	Matrix< T, M, N > result( *this);
	return result /= aScalar;
}
/**
 *
 */
template< class T, std::size_t M, std::size_t N >
Matrix< T, M, N >& Matrix< T, M, N >::operator+=( const Matrix< T, M, N >& rhs)
{
	for (std::size_t row = 0; row < M; ++row)
	{
		for (std::size_t column = 0; column < N; ++column)
		{
			matrix[row][column] += rhs.at( row, column);
		}
	}
	return *this;
}
/**
 *
 */
template< class T, std::size_t M, std::size_t N >
Matrix< T, M, N > Matrix< T, M, N >::operator+( const Matrix< T, M, N >& rhs) const
{
	Matrix< T, M, N > result( *this);
	return result += rhs;
}
/**
 *
 */
template< class T, std::size_t M, std::size_t N >
Matrix< T, M, N >& Matrix< T, M, N >::operator-=( const Matrix< T, M, N >& rhs)
{
	for (std::size_t row = 0; row < M; ++row)
	{
		for (std::size_t column = 0; column < N; ++column)
		{
			matrix[row][column] -= rhs.at( row, column);
		}
	}
	return *this;
}
/**
 *
 */
template< class T, std::size_t M, std::size_t N >
Matrix< T, M, N > Matrix< T, M, N >::operator-( const Matrix< T, M, N >& rhs) const
{
	Matrix< T, M, N > result( *this);
	return result -= rhs;
}
/**
 * (M, N) * (N, P) -> (M, P)
 */
template< typename T, std::size_t M, std::size_t N >
template< std::size_t columns >
Matrix< T, M, columns > Matrix< T, M, N >::operator*( const Matrix< T, N, columns >& rhs) const
{
	Matrix< T, M, columns > result;
	for(std::size_t currentResultColumn = 0; currentResultColumn < columns; currentResultColumn++) {
		for(std::size_t currentResultRow = 0; currentResultRow < M; currentResultRow++) {
			std::size_t rhsRow = 0;
			T value = 0;
			for(std::size_t lhsColumn = 0; lhsColumn < N; lhsColumn++) {
				value += (rhs.at(rhsRow, currentResultColumn) * matrix[currentResultRow][lhsColumn]);
				rhsRow++;
			}
			result.at(currentResultRow, currentResultColumn) = value;
		}
	}
	return result;
}
/**
 *
 */
template< class T, std::size_t M, std::size_t N >
Matrix< T, N, M > Matrix< T, M, N >::transpose() const
{
	Matrix< T, N, M > result;
	for(std::size_t i = 0; i < M; i++) {
		for (std::size_t y = 0; y < N; y++) {
			result.at(y, i) = matrix[i][y];
		}
	}
	return result;
}
/**
 *
 */
template< class T, std::size_t M, std::size_t N >
Matrix< T, M, N > Matrix< T, M, N >::identity() const
{
	int currentOnePosition = 0;
	Matrix< T, M, N > result;
	for(std::size_t i = 0; i < M; i++) {
		result.at(i, currentOnePosition) = 1;
		currentOnePosition++;
	}
	return result;
}
/**
 *
 */
template< class T, std::size_t M, std::size_t N >
Matrix< T, M, N > Matrix< T, M, N >::gauss() const
{
	// TODO Implement this function
	Matrix< T, M, N > result = *this;

	// we switch rows, so we don't start with a 0 as pivit point
	for(std::size_t i = 0; i < M; i++) {
		if(result.at(i, i) == 0) { // look for pivit point
			std::size_t rowLargestNumber = i;
			for (std::size_t y = i; y < M; y++) {
				if(result.at(y, i) > result.at(i, i)) {
					rowLargestNumber = y;
				}
			}
			std::array <T, N > tmp = result.at(i);
			result.at(i) = result.at(rowLargestNumber);
			result.at(rowLargestNumber) = tmp;
		}
	}

	std::size_t number = 0;

	for(std::size_t i = 0; i < M; i++) {
		T valueToDivide = result.at(i, i);

		for(std::size_t y = 0; y < N; y++) {
			result.at(i, y) = result.at(i, y) / valueToDivide;
		}

		for(std::size_t y = i+1; y < M; y++) {
			T valueToGetZero = result.at(y, number) / result.at(i, number);
			for(std::size_t k = 0; k < N; k++) {
				result.at(y, k) = result.at(y, k) - (valueToGetZero * result.at(i, k));
			}
		}

		number++;
	}

	return result;
}
/**
 *
 */
template< class T, std::size_t M, std::size_t N >
Matrix< T, M, N > Matrix< T, M, N >::gaussJordan() const
{
	// TODO Implement this function
	//throw std::logic_error("Not implemented yet");

	Matrix< T, M, N > result = *this;

	for(std::size_t i = 0; i < M; i++) {
		if(result.at(i, i) == 0) { // look for pivit point
			std::size_t rowLargestNumber = i;
			for (std::size_t y = i; y < M; y++) {
				if(result.at(y, i) > result.at(i, i)) {
					rowLargestNumber = y;
				}
			}
			std::array <T, N > tmp = result.at(i);
			result.at(i) = result.at(rowLargestNumber);
			result.at(rowLargestNumber) = tmp;
		}
	}

	for(std::size_t i = 0; i < M; i++) {

		T valueToDivide = result.at(i, i);

		for(std::size_t y = 0; y < N; y++) {
			result.at(i, y) = result.at(i, y) / valueToDivide;
		}

		for(std::size_t row = 0; row < M; row++) {
			if(row != i) {
				T valueToGetZero;
				for(std::size_t number = 0; number < N; number++) {
					if(identity().at(row, number) != 1 && result.at(i, number) != 0) {
						valueToGetZero = result.at(row, number) / result.at(i, number);
						break;
					}
				}

				for(std::size_t number = 0; number < N; number++) {
					result.at(row, number) = result.at(row, number) - (valueToGetZero * result.at(i, number));
				}
			}
		}
	}

	return result;
}
/**
 *

 */
template< class T, std::size_t M, std::size_t N >
Matrix< T, M, 1 > Matrix< T, M, N >::solve() const
{
	// TODO Implement this function
	Matrix < T, M, 1 > result;
	Matrix < T, M, N > solved = gaussJordan();

	for(size_t i = 0; i < M; i++) {
		result.at(i, 0) = solved.at(i, N-1);
	}

	return result;
}
/**
 *
 */
template< class T, std::size_t M, std::size_t N >
Matrix< T, M, N > Matrix< T, M, N >::inverse() const
{
	// TODO Implement this function

	Matrix< T, M, N > result = *this;

	Matrix< T, M, N > identityMatrix = identity();

	for(std::size_t i = 0; i < M; i++) {
		if(result.at(i, i) == 0) { // look for pivit point
			std::size_t rowLargestNumber = i;
			for (std::size_t y = i; y < M; y++) {
				if(result.at(y, i) > result.at(i, i)) {
					rowLargestNumber = y;
				}
			}
			std::array <T, N > tmp = result.at(i);
			std::array <T, N > tmpIdentity = identityMatrix.at(i);
			result.at(i) = result.at(rowLargestNumber);
			identityMatrix.at(i) = identityMatrix.at(rowLargestNumber);
			result.at(rowLargestNumber) = tmp;
			identityMatrix.at(rowLargestNumber) = tmpIdentity;
		}
	}

	for(std::size_t i = 0; i < M; i++) {

		T valueToDivide = result.at(i, i);

		for(std::size_t y = 0; y < N; y++) {
			result.at(i, y) = result.at(i, y) / valueToDivide;
			identityMatrix.at(i, y) = identityMatrix.at(i, y) / valueToDivide;
		}

		for(std::size_t row = 0; row < M; row++) {
			if(row != i) {
				T valueToGetZero;
				for(std::size_t number = 0; number < N; number++) {
					if(identity().at(row, number) != 1 && result.at(i, number) != 0) {
						valueToGetZero = result.at(row, number) / result.at(i, number);
						break;
					}
				}

				for(std::size_t number = 0; number < N; number++) {
					result.at(row, number) = result.at(row, number) - (valueToGetZero * result.at(i, number));
					identityMatrix.at(row, number) = identityMatrix.at(row, number) - (valueToGetZero * identityMatrix.at(i, number));
				}
			}
		}
	}

	return identityMatrix;
}
/**
 *
 */
template< class T, std::size_t M, std::size_t N >
std::string Matrix< T, M, N >::to_string() const
{
	std::string result = "Matrix<" + std::to_string( N) + "," + std::to_string( M) + ">\n{\n";
	for (std::size_t i = 0; i < M; ++i)
	{
		for (std::size_t j = 0; j < N; ++j)
		{
			result += std::to_string( matrix[i][j]) + ",";
		}
		result += "\n";
	}
	result += "}";
	return result;
}
/**
 *
 */
template< typename T, const std::size_t N >
bool equals(	const Matrix< T, 1, N >& lhs,
				const Matrix< T, 1, N >& rhs,
				const T aPrecision /*= std::numeric_limits<T>::epsilon()*/,
				const unsigned long aFactor /*= 1*/)
{
	// TODO Implement this function
	//throw std::logic_error("Not implemented yet");
	T precision = aPrecision * aFactor;

	for(std::size_t i = 0; i < N; i++) {
		if(lhs.at(0, i) - rhs.at(0, i) > precision || lhs.at(0, i) - rhs.at(0, i) < (-1 * precision)) {
			return false;
		}
	}
	return true;
}
/**
 *
 */
template< typename T, const std::size_t M >
bool equals(	const Matrix< T, M, 1 >& lhs,
				const Matrix< T, M, 1 >& rhs,
				const T aPrecision /*= std::numeric_limits<T>::epsilon()*/,
				const unsigned long aFactor /*= 1*/)
{
	// TODO Implement this function
	//throw std::logic_error("Not implemented yet");

	T precision = aPrecision * aFactor;

	for(std::size_t i = 0; i < M; i++) {
		if(lhs.at(i, 0) - rhs.at(i, 0) > precision || lhs.at(i, 0) - rhs.at(i, 0) < (-1 * precision)) {
			return false;
		}
	}

	return true;
}
/**
 *
 */
template< typename T, const std::size_t M, const std::size_t N >
bool equals(	const Matrix< T, M, N >& lhs,
				const Matrix< T, M, N >& rhs,
				const T aPrecision /*= std::numeric_limits<T>::epsilon()*/,
				const unsigned long aFactor /*= 1*/)
{
	// TODO Implement this function
	//throw std::logic_error("Not implemented yet");

	T precision = aPrecision * aFactor;

	for(std::size_t i = 0; i < M; i++) {
		for(std::size_t y = 0; y < N; y++) {
			if(lhs.at(i, y) - rhs.at(i, y) > precision || lhs.at(i, y) - rhs.at(i, y) < (-1 * precision)) {
				return false;
			}
		}
	}

	return true;
}

#ifndef ROBOT_CONTROLLER_HPP__
#define ROBOT_CONTROLLER_HPP__

#include <memory>

#include "Point.hpp"
#include "Kalman/KalmanEnvironment.hpp"
#include "Kalman/KalmanModel.hpp"
#include "Matrix.hpp"
#include "RobotModel.hpp"
#include "Environment.hpp"

class RobotController;
typedef std::shared_ptr<RobotController> RobotControllerPtr;

class RobotController
{
public:
    RobotController(){};    
    virtual ~RobotController(){};
    virtual void run(const Point& position) = 0;

    virtual const RobotModelPtr getModel() const = 0;
    virtual const EnvironmentPtr getEnvironment() const = 0;
};

#endif
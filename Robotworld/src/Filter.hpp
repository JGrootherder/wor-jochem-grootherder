#ifndef FILTER_HPP__
#define FILTER_HPP__

#include <memory>

class Filter;
typedef std::shared_ptr<Filter> FilterPtr;

class Filter
{
    Filter(){};
    virtual ~Filter(){};
};


#endif